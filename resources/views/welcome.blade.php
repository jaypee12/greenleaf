<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Green Leaf') }}</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
        <script src="{{ mix('/js/app.js') }}" defer></script>
    </head>
    <body id="page-top">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
          <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
            <img class="logo" src="{{asset('img/logos/gleaf-logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              Menu
              <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#location">Location</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#about">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#modelhouse">Model Houses</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#team">Amenities</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#contact">Contact Us</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      
        <!-- Header -->
        <header class="masthead">
          <div class="container">
              <div class="cover-bg"></div>
            <div class="intro-text">
              <div class="intro-heading text-uppercase "><b>Green Leaf Residences</b></div>
              <hr class="ht-4">
              <div class="intro-lead-in">Discover new homes, neigborhood and more.</div>
              <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#location">Inquire Now</a>
            </div>
          </div>
        </header>
      
        <!-- Services -->
        <section class="page-section mt-5" id="location">
          <div class="container mt-5">
            <div class="row">
              <div class="col-lg-12 text-center ">
                <h2 class="section-heading text-uppercase "><i class="fas fa-map-marked-alt green"></i> <br> Our Location</h2>
                <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
              </div>
            </div>
            <div class="row text-center">
              <div class="col-6">
                <img src="img/location-map.png" alt="map" class="img-thumbnail">
              </div>
              <div class="col-6">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
              </div>
            </div>
          </div>
        </section>
      

        <!-- About -->
        <section class="page-section" id="about">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase"><i class="fas fa-users green"></i> <br> About us</h2>
                <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <ul class="timeline">
                  <li>
                    <div class="timeline-image">
                      <img class="rounded-circle img-fluid h-100" src="img/amenities/amenity-1.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                      <div class="timeline-heading">
                        {{-- <h4>2009-2011</h4> --}}
                        <h4 class="subheading">Year 1</h4>
                      </div>
                      <div class="timeline-body">
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                      </div>
                    </div>
                  </li>
                  <li class="timeline-inverted">
                    <div class="timeline-image">
                      <img class="rounded-circle img-fluid h-100" src="img/amenities/amenity-2.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                      <div class="timeline-heading">
                        {{-- <h4>March 2011</h4> --}}
                        <h4 class="subheading">Year 2</h4>
                      </div>
                      <div class="timeline-body">
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="timeline-image">
                      <img class="rounded-circle img-fluid h-100 w-100" src="img/amenities/amenity-3.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                      <div class="timeline-heading">
                        {{-- <h4>December 2012</h4> --}}
                        <h4 class="subheading">Year 3</h4>
                      </div>
                      <div class="timeline-body">
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                      </div>
                    </div>
                  </li>
                  <li class="timeline-inverted">
                    <div class="timeline-image">
                      <img class="rounded-circle img-fluid h-100" src="img/amenities/amenity-4.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                      <div class="timeline-heading">
                        {{-- <h4>July 2014</h4> --}}
                        <h4 class="subheading">Year 4</h4>
                      </div>
                      <div class="timeline-body">
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                      </div>
                    </div>
                  </li>
                  <li class="timeline-inverted">
                    <div class="timeline-image">
                      <h4>Be Part
                        <br>Of Our
                        <br>Story!</h4>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
      
        <!-- Model Houses Grid -->
        <section class="bg-light page-section" id="modelhouse">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 text-center">
                  <h2 class="section-heading text-uppercase"><i class="fas fa-home green"></i> <br> Model Houses</h2>
                  <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-6 modelhouse-item">
                  <a class="modelhouse-link" data-toggle="modal" href="#modelhouseModal1">
                    <div class="modelhouse-hover">
                      <div class="modelhouse-hover-content">
                        <i class="fas fa-eye fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src="img/houses/model-1.jpg" alt="">
                  </a>
                  <div class="modelhouse-caption">
                    <h4>House 1</h4>
                    <p class="text-muted">Lorem ipsum dolor</p>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 modelhouse-item">
                  <a class="modelhouse-link" data-toggle="modal" href="#modelhouseModal2">
                    <div class="modelhouse-hover">
                      <div class="modelhouse-hover-content">
                        <i class="fas fa-eye fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src="img/houses/model-2.jpg" alt="">
                  </a>
                  <div class="modelhouse-caption">
                    <h4>House 2</h4>
                    <p class="text-muted">Lorem ipsum dolor</p>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 modelhouse-item">
                  <a class="modelhouse-link" data-toggle="modal" href="#modelhouseModal3">
                    <div class="modelhouse-hover">
                      <div class="modelhouse-hover-content">
                        <i class="fas fa-eye fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src="img/houses/model-3.jpg" alt="">
                  </a>
                  <div class="modelhouse-caption">
                    <h4>House 3</h4>
                    <p class="text-muted">Lorem ipsum dolor</p>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 modelhouse-item">
                  <a class="modelhouse-link" data-toggle="modal" href="#modelhouseModal4">
                    <div class="modelhouse-hover">
                      <div class="modelhouse-hover-content">
                        <i class="fas fa-eye fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src="img/houses/model-4.jpg" alt="">
                  </a>
                  <div class="modelhouse-caption">
                    <h4>House 4</h4>
                    <p class="text-muted">Lorem ipsum dolor</p>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 modelhouse-item">
                  <a class="modelhouse-link" data-toggle="modal" href="#modelhouseModal5">
                    <div class="modelhouse-hover">
                      <div class="modelhouse-hover-content">
                        <i class="fas fa-eye fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src="img/houses/model-5.jpg" alt="wew">
                  </a>
                  <div class="modelhouse-caption">
                    <h4>House 5</h4>
                    <p class="text-muted">Lorem ipsum dolor</p>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 modelhouse-item">
                  <a class="modelhouse-link" data-toggle="modal" href="#modelhouseModal6">
                    <div class="modelhouse-hover">
                      <div class="modelhouse-hover-content">
                        <i class="fas fa-eye fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src="img/houses/model-6.jpg" alt="">
                  </a>
                  <div class="modelhouse-caption">
                    <h4>House 6</h4>
                    <p class="text-muted">Lorem ipsum dolor</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 text-center">
                  <button class="btn btn-primary">View More</button>
                </div>
              </div>
            </div>
          </section>
        

        <!-- Team -->
        <section class="bg-light page-section" id="team">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase"><i class="fas fa-swimmer green"></i><br>Our Amenities</h2>
                <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-3">
                <div class="team-member">
                  <img class="mx-auto rounded-circle" src="img/amenities/amenity-1.jpg" alt="">
                  <h4>Club House</h4>
                  <p class="text-muted">Enjoy and Relax</p>
                  
                </div>
              </div>
              <div class="col-sm-3">
                <div class="team-member">
                  <img class="mx-auto rounded-circle" src="img/amenities/amenity-2.jpg" alt="">
                  <h4>Playground</h4>
                  <p class="text-muted">Play with your kids</p>
                  
                </div>
              </div>
              <div class="col-sm-3">
                <div class="team-member">
                  <img class="mx-auto rounded-circle" src="img/amenities/amenity-3.jpg" alt="">
                  <h4>Swimming Pool</h4>
                  <p class="text-muted">Beat the heat</p>
                  
                </div>
              </div>
              <div class="col-sm-3">
                <div class="team-member">
                  <img class="mx-auto rounded-circle" src="img/amenities/amenity-4.jpg" alt="">
                  <h4>Tennis Ground</h4>
                  <p class="text-muted">Entertain yourself</p>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-8 mx-auto text-center">
                <p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
              </div>
            </div>
          </div>
        </section>
      
    
        <!-- Contact -->
        <section class="page-section" id="contact">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Contact Us</h2>
                <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <form id="contactForm" name="sentMessage" novalidate="novalidate">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input class="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">
                        <p class="help-block text-danger"></p>
                      </div>
                      <div class="form-group">
                        <input class="form-control" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
                        <p class="help-block text-danger"></p>
                      </div>
                      <div class="form-group">
                        <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number.">
                        <p class="help-block text-danger"></p>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                        <p class="help-block text-danger"></p>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 text-center">
                      <div id="success"></div>
                      <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      
        <!-- Footer -->
        <footer class="footer">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-md-4">
                <span class="copyright">Copyright &copy; Green Leaf 2019</span>
              </div>
              <div class="col-md-4">
                <ul class="list-inline social-buttons">
                  <li class="list-inline-item">
                    <a href="#">
                      <i class="fab fa-twitter"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#">
                      <i class="fab fa-facebook"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#">
                      <i class="fab fa-linkedin-in"></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-md-4">
                <ul class="list-inline quicklinks">
                  <li class="list-inline-item">
                    <a href="#">Privacy Policy</a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#">Terms of Use</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      
        <!-- modelhouse Modals -->
      
        <!-- Modal 1 -->
        <div class="modelhouse-modal modal fade" id="modelhouseModal1" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                  <div class="rl"></div>
                </div>
              </div>
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                      <!-- Project Details Go Here -->
                      <h2 class="text-uppercase">House 1</h2>
                      <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                      <img class="img-fluid d-block mx-auto" src="img/houses/model-1.jpg" alt="">
                      <p>Use this area to describe your house. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
        <!-- Modal 2 -->
        <div class="modelhouse-modal modal fade" id="modelhouseModal2" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                  <div class="rl"></div>
                </div>
              </div>
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                      <!-- Project Details Go Here -->
                      <h2 class="text-uppercase">Project Name</h2>
                      <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                      <img class="img-fluid d-block mx-auto" src="img/houses/model-2.jpg" alt="">
                      <p>Use this area to describe your house. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
        <!-- Modal 3 -->
        <div class="modelhouse-modal modal fade" id="modelhouseModal3" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                  <div class="rl"></div>
                </div>
              </div>
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                      <!-- Project Details Go Here -->
                      <h2 class="text-uppercase">Project Name</h2>
                      <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                      <img class="img-fluid d-block mx-auto" src="img/houses/model-3.jpg" alt="">
                      <p>Use this area to describe your house. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
        <!-- Modal 4 -->
        <div class="modelhouse-modal modal fade" id="modelhouseModal4" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                  <div class="rl"></div>
                </div>
              </div>
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                      <!-- Project Details Go Here -->
                      <h2 class="text-uppercase">Project Name</h2>
                      <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                      <img class="img-fluid d-block mx-auto" src="img/houses/model-4.jpg" alt="">
                      <p>Use this area to describe your house. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
        <!-- Modal 5 -->
        <div class="modelhouse-modal modal fade" id="modelhouseModal5" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                  <div class="rl"></div>
                </div>
              </div>
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                      <!-- Project Details Go Here -->
                      <h2 class="text-uppercase">Project Name</h2>
                      <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                      <img class="img-fluid d-block mx-auto" src="img/houses/model-5.jpg" alt="">
                      <p>Use this area to describe your house. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
        <!-- Modal 6 -->
        <div class="modelhouse-modal modal fade" id="modelhouseModal6" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                  <div class="rl"></div>
                </div>
              </div>
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                      <!-- Project Details Go Here -->
                      <h2 class="text-uppercase">Project Name</h2>
                      <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                      <img class="img-fluid d-block mx-auto" src="img/houses/model-6.jpg" alt="">
                      <p>Use this area to describe your house. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      

      
      </body>
      
</html>
